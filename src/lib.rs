//! A Lua 5.3 parser crate.
//!
//! To get started:
//!
//! ```rust
//! use luaparse::{parse, HasSpan};
//! use luaparse::error::Error;
//!
//! let buf = r#"
//! local a = 42
//! local b = 24
//!
//! for i = 1, 100, 1 do
//!   b = a - b + i
//! end
//!
//! print(b)
//! "#;
//!
//! match parse(buf) {
//!     Ok(block) => println!("{}", block),
//!     Err(e) => eprintln!("{:#}", Error::new(e.span(), e).with_buffer(buf)),
//! }
//! ```

#![warn(future_incompatible, rust_2018_idioms, unused)]

pub mod ast;
pub mod error;
pub mod token;

mod cursor;
mod lexer;
mod parser;
mod span;
mod visitor;
mod printer;
mod symbol;

pub use crate::cursor::InputCursor;
pub use crate::lexer::{LexerErrorKind, LexerError, Lexer};
pub use crate::parser::{ParseError, Parser};
pub use crate::span::{HasSpan, Span, Position};
pub use crate::visitor::{AstDescend, VisitorMut};

/// Parses the input as a Lua chunk.
pub fn parse(buf: &str) -> Result<crate::ast::Block<'_>, ParseError<'_>> {
    Parser::new(Lexer::new(InputCursor::new(buf))).chunk()
}
