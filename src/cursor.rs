//! An input cursor.

use std::ops::{Bound, RangeBounds};
use std::str::Chars;

use crate::span::Position;

/// An input cursor.
#[derive(Clone, Debug)]
pub struct InputCursor<'a> {
    buf: &'a str,
    iter: Chars<'a>,
    position: Position,
    prev_position: Option<Position>,
    newline: bool,
    keep_column: bool,
    eof: bool,
}

impl<'a> InputCursor<'a> {
    /// Create a new cursor from a buffer.
    pub fn new(buf: &'a str) -> Self {
        Self {
            buf,
            iter: buf.chars(),
            position: Position {
                byte: 0,
                line: 1,
                col: 1,
            },
            prev_position: None,
            newline: false,
            keep_column: false,
            eof: false,
        }
    }

    /// Returns the `n`th character after the cursor without advancing the cursor.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// let cursor = InputCursor::new("hello world!");
    /// assert_eq!(cursor.peek_nth(0), Some('h'));
    /// assert_eq!(cursor.peek_nth(2), Some('l'));
    /// ```
    pub fn peek_nth(&self, n: usize) -> Option<char> {
        self.iter.clone().nth(n)
    }

    /// Returns the next character without advancing the cursor. Equivalent to `peek_nth(0)`.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// let cursor = InputCursor::new("hello world!");
    /// assert_eq!(cursor.peek(), Some('h'));
    /// assert_eq!(cursor.peek(), cursor.peek_nth(0));
    /// ```
    pub fn peek(&self) -> Option<char> {
        self.peek_nth(0)
    }

    /// Returns the position of the following character.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// # use luaparse::Position;
    /// let mut cursor = InputCursor::new("hello\nworld");
    /// assert_eq!(cursor.pos(), Position {
    ///     byte: 0,
    ///     line: 1,
    ///     col: 1,
    /// });
    /// cursor.skip_n(5);
    /// assert_eq!(cursor.pos(), Position {
    ///     byte: 5,
    ///     line: 1,
    ///     col: 6,
    /// });
    /// cursor.next();
    /// assert_eq!(cursor.pos(), Position {
    ///     byte: 6,
    ///     line: 2,
    ///     col: 1,
    /// });
    /// ```
    pub fn pos(&self) -> Position {
        if self.newline {
            Position {
                byte: self.position.byte,
                line: self.position.line + 1,
                col: 1,
            }
        } else {
            self.position
        }
    }

    /// Returns the position of the following character.
    ///
    /// Only differs from `pos` when the cursor is immediately after a newline character.
    /// In that case, unlike `pos`, this method does not go to the next line.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// # use luaparse::Position;
    /// let mut cursor = InputCursor::new("hello\nworld");
    /// assert_eq!(cursor.pos(), Position {
    ///     byte: 0,
    ///     line: 1,
    ///     col: 1,
    /// });
    /// cursor.skip_n(5);
    /// assert_eq!(cursor.pos(), Position {
    ///     byte: 5,
    ///     line: 1,
    ///     col: 6,
    /// });
    /// cursor.next();
    /// assert_eq!(cursor.pos(), Position {
    ///     byte: 6,
    ///     line: 1,
    ///     col: 7,
    /// });
    /// ```
    pub fn pos_no_newline(&self) -> Position {
        self.position
    }

    /// Returns the position of the preceding character.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// # use luaparse::Position;
    /// let mut cursor = InputCursor::new("hello\nworld");
    /// assert_eq!(cursor.prev_pos(), Position {
    ///     byte: 0,
    ///     line: 1,
    ///     col: 1,
    /// });
    /// cursor.skip_n(5);
    /// assert_eq!(cursor.prev_pos(), Position {
    ///     byte: 4,
    ///     line: 1,
    ///     col: 5,
    /// });
    /// cursor.next();
    /// assert_eq!(cursor.prev_pos(), Position {
    ///     byte: 5,
    ///     line: 1,
    ///     col: 6,
    /// });
    /// ```
    pub fn prev_pos(&self) -> Position {
        self.prev_position.unwrap_or(Position {
            byte: 0,
            line: 1,
            col: 1,
        })
    }

    /// Returns the remaining string as a subslice.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// let mut cursor = InputCursor::new("hello world");
    /// cursor.skip_n(6);
    /// assert_eq!(cursor.remaining(), "world");
    /// ```
    pub fn remaining(&self) -> &'a str {
        self.iter.as_str()
    }

    /// Returns a substring of the buffer.
    ///
    /// Accepts a range defined by two `Position`s.
    ///
    /// The positions must be previously obtained using `pos` or its variant. The function may
    /// panic if the position is invalid.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// let mut cursor = InputCursor::new("hello world!");
    /// let start = cursor.pos();
    /// cursor.skip_n(5);
    /// let end = cursor.prev_pos();
    /// assert_eq!(cursor.substr(start..=end), "hello");
    /// ```
    pub fn substr<I>(&self, range: I) -> &'a str
    where
        I: RangeBounds<Position>,
    {
        use Bound::*;

        let char_len_at = |b: usize| {
            self.buf[b..]
                .chars()
                .next()
                .map(char::len_utf8)
                .unwrap_or(0)
        };

        let start = match range.start_bound() {
            Included(pos) => Included(pos.byte),
            Excluded(pos) => Included(pos.byte + char_len_at(pos.byte)),
            Unbounded => Unbounded,
        };

        let end = match range.end_bound() {
            Included(pos) => Excluded(pos.byte + char_len_at(pos.byte)),
            Excluded(pos) => Excluded(pos.byte),
            Unbounded => Unbounded,
        };

        match (start, end) {
            (Included(s), Excluded(e)) => &self.buf[s..e],
            (Included(s), Unbounded) => &self.buf[s..],
            (Unbounded, Excluded(e)) => &self.buf[..e],
            (Unbounded, Unbounded) => &self.buf[..],
            _ => unreachable!(),
        }
    }

    /// Advances the cursor by `n` characters.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// let mut cursor = InputCursor::new("hello world!");
    /// cursor.skip_n(6);
    /// assert_eq!(cursor.remaining(), "world!");
    /// ```
    pub fn skip_n(&mut self, n: usize) {
        for _ in 0..n {
            self.next();
        }
    }

    /// Consumes `n` characters starting from the current position.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// let mut cursor = InputCursor::new("hello world!");
    /// let s = cursor.consume_n(5);
    /// assert_eq!(s, "hello");
    /// assert_eq!(cursor.remaining(), " world!");
    /// ```
    pub fn consume_n(&mut self, n: usize) -> &'a str {
        let start = self.pos();
        self.skip_n(n);
        let end = self.pos();

        self.substr(start..end)
    }

    /// Consumes `n` characters starting from the current position if the consumed string satisfies
    /// a predicate.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// let mut cursor = InputCursor::new("hello world!");
    /// assert_eq!(cursor.consume_n_if(5, |s| s.chars().all(|c| c.is_ascii_alphanumeric())), Some("hello"));
    /// assert_eq!(cursor.remaining(), " world!");
    /// assert_eq!(cursor.consume_n_if(5, |s| s.chars().all(|c| c.is_ascii_alphanumeric())), None);
    /// assert_eq!(cursor.remaining(), " world!");
    /// assert_eq!(cursor.consume_n_if(100, |_| true), None);
    /// ```
    pub fn consume_n_if<P>(&mut self, n: usize, predicate: P) -> Option<&'a str>
    where
        P: FnOnce(&str) -> bool,
    {
        let iter = self.iter.clone().take(n);
        let count = iter.clone().count();
        let len = iter.map(char::len_utf8).sum();
        let start = self.pos();
        let s = &self.remaining()[..len];

        if count == n && predicate(s) {
            self.skip_n(n);
            Some(&self.substr(start..self.pos()))
        } else {
            None
        }
    }

    /// Consumes the following character if it satisfies the predicate.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// let mut cursor = InputCursor::new("hello world!");
    /// assert_eq!(cursor.consume_if(|c| c.is_ascii_alphanumeric()), Some('h'));
    /// assert_eq!(cursor.consume_if(|c| c == ' '), None);
    /// ```
    pub fn consume_if<P>(&mut self, predicate: P) -> Option<char>
    where
        P: FnOnce(char) -> bool,
    {
        if predicate(self.peek()?) {
            self.next()
        } else {
            None
        }
    }

    /// Consumes characters while the predicate is satisfied. Returns `None` if the string is
    /// empty.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// let mut cursor = InputCursor::new("hello\nworld!");
    /// assert_eq!(cursor.consume_while(|c| c != '\n'), Some("hello"));
    /// assert_eq!(cursor.consume_while(|c| c != '\n'), None);
    /// assert_eq!(cursor.next(), Some('\n'));
    /// ```
    pub fn consume_while<P>(&mut self, mut predicate: P) -> Option<&'a str>
    where
        P: FnMut(char) -> bool,
    {
        let n = self.iter.clone().take_while(|&c| predicate(c)).count();

        if n == 0 {
            return None;
        }

        let start = self.pos();
        self.skip_n(n);

        Some(self.substr(start..self.pos()))
    }

    /// Consumes no more than `n` characters while they satisfy the predicate. Returns `None` if
    /// the string is empty.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// let mut cursor = InputCursor::new("hello\nworld!");
    /// assert_eq!(cursor.consume_first_n_while(4, |c| c != '\n'), Some("hell"));
    /// assert_eq!(cursor.consume_first_n_while(4, |c| c != '\n'), Some("o"));
    /// assert_eq!(cursor.consume_first_n_while(4, |c| c != '\n'), None);
    /// ```
    pub fn consume_first_n_while<P>(&mut self, n: usize, mut predicate: P) -> Option<&'a str>
    where
        P: FnMut(char) -> bool,
    {
        let n = self
            .iter
            .clone()
            .take_while(|&c| predicate(c))
            .take(n)
            .count();

        if n == 0 {
            return None;
        }

        let start = self.pos();
        self.skip_n(n);

        Some(self.substr(start..self.pos()))
    }

    /// Checks whether the remaining string starts with the given substring.
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// let mut cursor = InputCursor::new("hello\nworld!");
    /// assert!(cursor.starts_with("hell"));
    /// cursor.skip_n(2);
    /// assert!(cursor.starts_with("llo\nw"));
    /// assert!(!cursor.starts_with("jelly"));
    /// ```
    pub fn starts_with(&self, s: &str) -> bool {
        self.remaining().starts_with(s)
    }

    /// Skips the characters until the next line. Returns the position of the character preceding
    /// the line terminator (`\r`, `\n`, `\r\n`, `\n\r`).
    ///
    /// ```rust
    /// # use luaparse::InputCursor;
    /// let mut cursor = InputCursor::new("hello\r\nworld!");
    /// let start = cursor.pos();
    /// let end = cursor.skip_line();
    /// assert_eq!(cursor.substr(start..=end), "hello");
    /// assert_eq!(cursor.remaining(), "world!");
    /// ```
    pub fn skip_line(&mut self) -> Position {
        self.consume_while(|c| c != '\r' && c != '\n');
        let end_pos = self.prev_pos();

        if self
            .consume_n_if(2, |s| s == "\r\n" || s == "\n\r")
            .is_none()
        {
            self.consume_if(|s| s == '\r' || s == '\n');
        }

        end_pos
    }
}

impl<'a> Iterator for InputCursor<'a> {
    type Item = char;

    fn next(&mut self) -> Option<char> {
        let c = self.iter.next();

        self.prev_position.replace(self.pos());
        self.position.byte += if let Some(c) = c { c.len_utf8() } else { 0 };

        if !self.eof {
            if self.newline {
                self.position.line += 1;
                self.position.col = 1;
                self.newline = false;
            }

            if self.keep_column {
                self.keep_column = false;
            } else {
                self.position.col += 1;
            }
        }

        if c.is_none() {
            self.eof = true;
        }

        let c = c?;

        match c {
            '\r' => match self.peek() {
                Some('\n') => self.keep_column = true,
                _ => self.newline = true,
            },
            '\n' => self.newline = true,
            _ => {}
        }

        Some(c)
    }
}
