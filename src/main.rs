use std::io::{self, Read};

use luaparse::{parse, HasSpan};
use luaparse::error::Error;

fn main() {
    loop {
        let mut input = String::new();

        if let Err(e) = io::stdin().read_to_string(&mut input) {
            if let io::ErrorKind::Interrupted = e.kind() {
                break;
            } else {
                panic!("{:?}", e);
            }
        }

        match parse(&input) {
            Ok(block) => println!("{}", block),
            Err(e) => eprintln!("{:#}", Error::new(e.span(), e).with_buffer(&input)),
        }
    }
}
