# luaparse
A Lua 5.3 parser. Preserves insignificant tokens (whitespace and comments) in
the syntax tree.

[Documentation](https://docs.rs/luaparse/0.1.0/luaparse/).

### Example
```rust
use luaparse::error::Error;
use luaparse::{parse, HasSpan};

let buf = r#"
local a = 42
local b = 24

for i = 1, 100, 1 do
  b = a - b + i
end

print(b)
"#;

match parse(buf) {
    Ok(block) => println!("{}", block),
    Err(e) => eprintln!("{:#}", Error::new(e.span(), e).with_buffer(buf)),
}
```
